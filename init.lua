minetest.register_on_newplayer(function(player)
  -- Return if skin tables not defined
  if not ( skins and skins.skins ) then return end 
  local name = player:get_player_name()
  -- Return if skin was already set for this player
  if skins.skins[name] then return end
  local len = string.len(name)
  local hash = 0
  local max_skin_count = tonumber(minetest.settings:get("player_hash_skin.max_count")) or 200
 
 	local h = {}
  -- prefix of textures files
	h.sprefix = "player_hash_skin_"
  -- mod providing textures files
	h.smodpath = minetest.get_modpath("player_hash_skin")
  -- start count of texture files
	h.s = 0

  for i = 1, len, 1 do
   hash = hash+(string.byte(name,i))
  end
	
  
	-- if simple_skins mod exist, use its skins
	local skinmod = minetest.get_modpath("simple_skins")
	if skinmod ~= nil then
	 	minetest.log("info", "[player_hash_skin] simple_skins mod detected")
		 h.smodpath = minetest.get_modpath("simple_skins")
		 h.sprefix = "character_"
		 h.s = 1
	end

	
	-- loop through the files and count them  
	h.n = h.s
	local fp
	while true do
		if h.n == max_skin_count then break end
		fp = io.open(h.smodpath .. "/textures/".. h.sprefix ..h.n.. ".png")
		if not fp then break end
		fp:close()
		h.n = h.n + 1
	end
	-- if count started at 1 need to remove 1
	h.n = h.n - h.s
	
 	minetest.log("info", "[player_hash_skin] "..h.n.." skins found")

	-- hash
 	hash = hash%h.n
 	minetest.log("info", "[player_hash_skin] hash =  "..hash..".")


	-- Open the file (to test its existence)
	local filename = h.smodpath.."/textures/"..h.sprefix..hash
	local f = io.open(filename..".png")
		
	if f then
		f:close()
		if skinmod then
			local skin_name = h.sprefix..hash
			skins.skins[name] = skin_name
			player:set_attribute("simple_skins:skin", skins.skins[name])
		else
			local img = h.sprefix.."%i.png"
			default.player_set_textures(player, {string.format(img, hash)})
		end
	end
end)
